package com.example.springbootsecuritydemo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("basic")
class BasicSecurityTests {

    @Autowired
    WebTestClient webTestClient;

    @Test
    void test_secured_book_success() {
        webTestClient.get()
                .uri("/books")
                .header("Authorization","Basic "+ Base64.getEncoder().encodeToString("admin:password".getBytes(StandardCharsets.UTF_8)))
                //.header("Authorization","Basic "+ "admin password")
                .exchange()
                .expectStatus().isOk()
                .expectBody(String.class)
                .isEqualTo("[{\"name\":\"Flanders\"},{\"name\":\"Flexnes\"}]");
    }

    @Test
    void test_secured_book_failed_because_authorization_failed() {
        webTestClient.get()
                .uri("/books")
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody(String.class)
                .isEqualTo(null);
    }

    @Test
    void test_secured_book_failed_because_insufficient_privileges() {
        webTestClient.get()
                .uri("/books")
                .header("Authorization","Basic "+ Base64.getEncoder().encodeToString("user:password".getBytes(StandardCharsets.UTF_8)))
                //.header("Authorization","Basic "+ "admin password")
                .exchange()
                .expectStatus().isForbidden();
    }

}
