package com.example.springbootsecuritydemo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("jwt")
class JwtSecurityTests {

    @Autowired
    WebTestClient webTestClient;

    KeyCloakToken userToken;
    KeyCloakToken adminToken;

    @BeforeEach
    void setUp() {
        userToken = KeyCloakToken.acquire("https://iam.sensera.se/", "test", "group-api", "user", "djnJnPf7VCQvp3Fc")
                .block();
        adminToken = KeyCloakToken.acquire("https://iam.sensera.se/", "test", "group-api", "admin", "43NfPkLcU5QMGhGA")
                .block();
    }

    @Test
    void test_secured_book_success() {
        webTestClient.get()
                .uri("/books")
                .header("Authorization","Bearer "+ adminToken.getAccessToken())
                //.header("Authorization","Basic "+ "admin password")
                .exchange()
                .expectStatus().isOk()
                .expectBody(String.class)
                .isEqualTo("[{\"name\":\"Flanders\"},{\"name\":\"Flexnes\"}]");
    }

    @Test
    void test_secured_book_failed_because_authorization_failed() {
        webTestClient.get()
                .uri("/books")
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody(String.class)
                .isEqualTo(null);
    }

    @Test
    void test_secured_book_failed_because_insufficient_privileges() {
        webTestClient.get()
                .uri("/books")
                .header("Authorization","Bearer "+ userToken.getAccessToken())
                .exchange()
                .expectStatus().isForbidden();
    }

}
