package com.example.springbootsecuritydemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("shelves")
public class ShelfController {

    @GetMapping
    public List<Shelf> all() {
        return List.of(
                Shelf.create("H3"),
                Shelf.create("A6")
        );
    }
}
