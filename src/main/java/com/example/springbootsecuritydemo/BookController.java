package com.example.springbootsecuritydemo;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("books")
@CrossOrigin("*")
//@CrossOrigin("http://localhost:3000")
public class BookController {

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ADMIN') or hasAnyRole('ADMIN')")
    public List<Book> all(Principal principal) {
        System.out.println("BookController.all principal.name="+principal);
        return List.of(
                Book.create("Flanders"),
                Book.create("Flexnes")
        );
    }
}
