package com.example.springbootsecuritydemo;

import lombok.Value;

@Value(staticConstructor = "create")
public class Shelf {
    String name;
}
